// Global app controller
// import str from './models/Search';
// // import {sum, mult, ID} from './views/SearchView';
// import * as searchView from './views/SearchView';

// console.log(`Imported functions: ${searchView.sum(searchView.ID, 2)} and ${searchView.mult(3,5)}. ${str}`);
import axios from 'axios';

async function getResults(query){
    const res = await axios(`https://forkify-api.herokuapp.com/api/search?&q=${query}`);
    const recipes = res.data.recipes;
    console.log(recipes);
}
getResults('lobster');

console.log('----------');